import turtle

def kwadrat(t,l):
    for i in range():
        t.forward(l)
        t.left(90)

def main():
    t = turtle.Turtle()
    turtle.Screen().bgcolor("green")
    t.color("pink")
    t.shape("turtle")

    for i in range(1,6):
        kwadrat(t,10*i)
        t.up()
        t.right(90)
        t.forward(10/2)
        t.right(90)
        t.forward(10/2)
        t.right(180)
        t.down()

main()
